# Softvision Las Vegas Studio : Node-Red-Alpine Docker Image
------------------------------------------------------------
This docker image is built on Alpine 3.6 with Node.js version 8.11.3 LTS. The running service is Node-Red v0.18.7

https://nodered.org/

- Node-RED is a programming tool for wiring together hardware devices, APIs, and online services in new and interesting ways.

- It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click.
------

#### usage:


```
docker run -d -p 1880:1880 --name <container_name> softvisionlv/node-red-alpine:latest
```

To access the shell, simply use:

```
docker exec -it <container_name> /bin/sh
```

#### Additional Plugins That Are Installed

Name | Version | NPM Link
---- | ------- | --------
node-red-contrib-stoptimer | 0.0.7 | https://www.npmjs.com/package/node-red-contrib-stoptimer
node-red-contrib-moment | 2.0.7 | https://www.npmjs.com/package/node-red-contrib-moment
node-red-contrib-simpletime | 2.0.5 | https://www.npmjs.com/package/node-red-contrib-simpletime
node-red-contrib-json | 0.2.0 |https://www.npmjs.com/package/node-red-contrib-mqtt-broker
node-red-dashboard | 2.9.4 | https://www.npmjs.com/package/node-red-dashboard
node-red-node-base64 | 0.1.3 | https://www.npmjs.com/package/node-red-node-base64
node-red-contrib-scheduler | 1.0.5 | https://www.npmjs.com/package/node-red-contrib-scheduler
node-red-contrib-httpauth | 1.0.12 | https://www.npmjs.com/package/node-red-contrib-httpauth
node-red-contrib-boolean-logic | 0.0.3 | https://www.npmjs.com/package/node-red-contrib-boolean-logic
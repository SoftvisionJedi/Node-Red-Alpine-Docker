FROM node:alpine

LABEL maintainer "Jairo Martinez <jmartinez@softvision.com>"
LABEL "ProductID"="svlv0002"
LABEL "ProductName"="Node-Red Service On Alpine Linux"
LABEL "ProductVersion"="0.1.0"
LABEL version="0.1.0"
LABEL name="softvisionlv/node-red-alpine"

WORKDIR /usr/app
COPY package*.json /usr/app/

RUN npm i -g --unsafe-perm node-red \
	&& echo "----------------------------------------------" \
	&& mkdir -v -p /root/.node-red \
	&& cp /usr/app/package.json /root/.node-red/package.json \
	&& cd /root/.node-red \
	&& echo "----------------------------------------------" \
	&& npm i --unsafe-perm \
	&& echo "----------------------------------------------"

CMD node-red
EXPOSE 1880